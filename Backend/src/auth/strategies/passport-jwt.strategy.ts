import * as dotenv from 'dotenv';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { StudentRepository } from '../../gestion-pfe/repositories/student.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { ProfessorRepository } from '../../gestion-pfe/repositories/professor.repository';
import { AdminRepository } from '../../gestion-pfe/repositories/admin.repository';

dotenv.config();

export class PassportJwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    @InjectRepository(StudentRepository)
    private studentRepository: StudentRepository,
    @InjectRepository(ProfessorRepository)
    private professorRepository: ProfessorRepository,
    @InjectRepository(AdminRepository)
    private adminRepository: AdminRepository,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.SECRET,
    });
  }

  async validate(payload) {
    //we can define the payload interface
    const student = await this.getStudent(payload);
    if (student) return student;
    const professor = await this.getProfessor(payload);
    if (professor) return professor;
    const admin = await this.getAdmin(payload);
    if (admin) return admin;
  }

  async getStudent(payload) {
    const user = await this.studentRepository.findOne({
      id: payload.id,
    });
    if (user) {
      delete user.password;
      return user; // this gets injected in request !
    }
  }

  async getProfessor(payload) {
    const user = await this.professorRepository.findOne({
      id: payload.id,
    });
    if (user) {
      delete user.password;
      user.role = 'professor';
      return user; // this gets injected in request !
    }
  }

  async getAdmin(payload) {
    const user = await this.adminRepository.findOne({
      id: payload.id,
    });
    if (user) {
      delete user.password;
      user.role = 'admin';
      return user; // this gets injected in request !
    }
  }
}
