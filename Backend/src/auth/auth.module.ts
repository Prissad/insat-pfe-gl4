import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdminRepository } from '../gestion-pfe/repositories/admin.repository';
import { ProfessorRepository } from '../gestion-pfe/repositories/professor.repository';
import { StudentRepository } from '../gestion-pfe/repositories/student.repository';
import { StudentAuthController } from './controllers/student.auth.controller';
import { ProfessorController } from './controllers/professor.controller';
import { AdminController } from './controllers/admin.controller';
import { StudentAuthService } from './services/student.auth/student.auth.service';
import { ProfessorAuthService } from './services/professor.auth/professor.auth.service';
import { AdminAuthService } from './services/admin.auth/admin.auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import * as dotenv from 'dotenv';
import { PassportJwtStrategy } from './strategies/passport-jwt.strategy';

dotenv.config();

@Module({
  imports: [
    TypeOrmModule.forFeature([
      StudentRepository,
      ProfessorRepository,
      AdminRepository,
    ]),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.SECRET,
      signOptions: {
        expiresIn: 3600,
      },
    }),
  ],
  controllers: [StudentAuthController, ProfessorController, AdminController],
  providers: [
    StudentAuthService,
    ProfessorAuthService,
    AdminAuthService,
    PassportJwtStrategy,
  ],
  exports: [PassportJwtStrategy, PassportModule],
})
export class AuthModule {}
