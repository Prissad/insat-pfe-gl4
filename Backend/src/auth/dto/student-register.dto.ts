import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';
import { UserSubscribeDto } from './user-subscribe.dto';

export class StudentRegisterDto extends UserSubscribeDto {
  @ApiProperty()
  @IsNotEmpty()
  carteEtudiant: string;
}
