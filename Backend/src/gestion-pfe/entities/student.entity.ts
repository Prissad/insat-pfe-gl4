import { Column, Entity } from 'typeorm';
import { UserEntity } from '../../generics/user.entity';

@Entity('student')
export class StudentEntity extends UserEntity {
  @Column()
  carteEtudiant: string;
}
